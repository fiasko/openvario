const gps = require('./bin/L80_Gmm-master/GPS_EXAMPLE').execute
const pressure = require('./bin/L80_Gmm-master/MS5637').execute
const io = require("socket.io")();
const socketapi = {
    io: io
};

// Add your socket.io logic here!
io.on("connection", function (socket) {
    console.log("A user connected");
});
// end of socket.io logic

gps(data => {
    io.emit("gps", data);
})

pressure(data => {
    io.emit("pressure", data);
})


module.exports = socketapi;