const { spawn } = require('child_process')
const readline = require('readline')
const path = require('path')

function execute(cb) {

    try {

        const process = spawn(path.join(__dirname, 'ms5637_funcs'), {
            stdio: "pipe"
        })

        readline.createInterface(process.stdout).on('line', line => {
            
            const pressure = +line;
            cb(pressure);

        })
    } catch {
        console.log("oh no!")
    }

}

module.exports = {
    execute
}
