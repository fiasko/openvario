const { spawn } = require('child_process')
const readline = require('readline')
const path = require('path')

function convertMinutes(angle) {

    angle = +angle;
    4846.9624

    let wholeDegrees = Math.floor(angle / 100);
    let minutes = angle - wholeDegrees * 100;

    return wholeDegrees + minutes / 60;

}

function execute(cb) {

    try {

        const process = spawn(path.join(__dirname, 'gps_example'), {
            stdio: "pipe"
        })

        readline.createInterface(process.stdout).on('line', line => {
            // line= $GPGGA,172043.000,4846.9624,N,00909.3985,E,2,8,1.15,294.8,M,48.0,M,,*5A
            if (line.startsWith("$GPGGA")) {
                let [header, utc, lat, latDir, lng, lngDir, quality, satellites, horizontalPrecision, altitude, altitudeUnits, undulation, age, stnId, chk] = line.split(',');

                if (cb != null) {
                    cb({
                        line,
                        header, 
                        utc, 
                        lat: convertMinutes(lat) * (latDir == 'W' ? -1 : 1), 
                        latDir, 
                        lng: convertMinutes(lng) * (lngDir == 'S' ? -1 : 1), 
                        lngDir, 
                        quality, 
                        satellites, 
                        horizontalPrecision, 
                        altitude, 
                        altitudeUnits, 
                        undulation, 
                        age, 
                        stnId, 
                        chk    
                    })
                }
            }

        })
    } catch {
        console.log("oh no!")
    }

}

module.exports = {
    execute
}
