import { Component, ElementRef, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { GliderService, GpsPosition } from '../glider.service';
import { filter, throttleTime } from 'rxjs/operators'
import { interpolateCool as colorScale } from 'd3-scale-chromatic'
import { ScaleLinear, scaleLinear } from 'd3-scale';

@Component({
  selector: '[app-map]',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  trace: GpsPosition[];
  polyline: any;
  map: L.Map;

  autoFitBounds = true;

  verticalSpeedScale: ScaleLinear<number, number, never>;

  constructor(private elRef: ElementRef<HTMLDivElement>, private glider: GliderService) {
    this.trace = [];
    this.verticalSpeedScale = scaleLinear().range([0, 1]).domain([-3, 3])

  }

  ngOnInit() {

    this.map = L.map(this.elRef.nativeElement, {
      center: [0, 0.0],
      zoom: 1
    });

    L.tileLayer('http://localhost:4200/hot/{z}/{x}/{y}.png', {
      tileSize: 512,
      zoomOffset: -1
    }).addTo(this.map)


    this.glider.gps.subscribe(next => {

      this.trace.push(next);
      if (this.trace.length > 250) {
        this.trace.splice(0, 1)
      }

      if (this.polyline != null) {
        this.polyline.remove();
      }
      this.polyline = L.polyline(this.trace, { color: 'red' }).addTo(this.map);

      if (this.autoFitBounds) {
        this.fit();
      }

    })

    this.glider.gps
      .pipe(
        throttleTime(1000))
      .subscribe(fix => this.addColor(fix))

    this.map.on('dragstart', () => {
      this.autoFitBounds = false;
    })

  }

  fit() {
    this.autoFitBounds = true;
    if (this.polyline != null) {
      this.map.fitBounds(this.polyline.getBounds(), {
        padding: new L.Point(100, 100)
      });
    }

  }

  addColor(fix: GpsPosition) {
    const c = colorScale;
    const circle = L.circle(fix, {
      radius: 5,
      fillColor: colorScale(this.verticalSpeedScale(fix.verticalVelocity)),
      stroke: false,
      fillOpacity: 0.8,
      className: "circle-transition"
    }).addTo(this.map);

    setTimeout(() => {
      
      circle.setStyle({
        fillOpacity: 0
      })

      setTimeout(() => {
        circle.remove()
      }, 2000);

    }, 60000)


  }

}
