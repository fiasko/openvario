import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VectorVariometerComponent } from './vector-variometer.component';

describe('VectorVariometerComponent', () => {
  let component: VectorVariometerComponent;
  let fixture: ComponentFixture<VectorVariometerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VectorVariometerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VectorVariometerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
