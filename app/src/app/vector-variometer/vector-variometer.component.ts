import { Component, OnInit } from '@angular/core';
import { GliderService } from '../glider.service';

@Component({
  selector: '[app-vector-variometer]',
  templateUrl: './vector-variometer.component.html',
  styleUrls: ['./vector-variometer.component.scss']
})
export class VectorVariometerComponent implements OnInit {

  constructor(public glider: GliderService) { }

  ngOnInit(): void {
  }

}
