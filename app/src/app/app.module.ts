import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { VectorSpeedometerComponent } from './vector-speedometer/vector-speedometer.component';
import { VectorVariometerComponent } from './vector-variometer/vector-variometer.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    VectorSpeedometerComponent,
    VectorVariometerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
