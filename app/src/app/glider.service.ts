import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import data from 'src/assets/data.js';
import { io, Socket } from "socket.io-client";
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';

const START_INDEX = 26;

export class Vector2 {

  constructor(public x: number, public y: number) { }

  add(a: Vector2) {
    return new Vector2(this.x + a.x, this.y + a.y);
  }

  scale(s: number) {
    return new Vector2(this.x * s, this.y * s);
  }

  rotate(angle: number) {
    return new Vector2(
      this.x * Math.cos(angle) - this.y * Math.sin(angle),
      this.x * Math.sin(angle) + this.y * Math.cos(angle)
    );
  }

  angleDeg() {
    return this.angleRad() * 180 / Math.PI
  }

  angleRad() {
    return Math.atan2(this.y, this.x)
  }

}

class FIR {
  buffer: number[]
  weights: number[];

  private totalWeight: number;

  constructor(size: number, initialValue: number = 0) {
    this.buffer = Array.from(Array(size).keys()).map(_ => initialValue)
    this.init();
  }

  init() {
    this.weights = this.buffer.map((_, index) => Math.exp(-index / 5))
    this.totalWeight = this.weights.reduce((prev, curr) => prev + curr);
  }

  pushValue(value: number) {
    this.buffer.unshift(value)
    this.buffer.splice(-1, 1);
    return this.getValue();
  }

  getValue() {
    return this.buffer.reduce((prev, curr, i) => prev + curr * this.weights[i], 0) / this.totalWeight;
  }
}

export interface GpsPosition {
  lat: number;
  lng: number;
  altitude?: number;
  bearing?: number;
  verticalVelocity?: number;
  timestamp?: number;
}

@Injectable({
  providedIn: 'root'
})
export class GliderService {

  groundSpeed = new Vector2(0, 0);
  airSpeed = new Vector2(0, 0);
  windSpeed = new Vector2(0, 0);

  bearing = 0;

  center = new Vector2(0, 0);

  gps: BehaviorSubject<GpsPosition>;


  currentGps: any;

  altitudeFIR: FIR;
  // bearingFIR: FIR;

  calculateDistance(p1: GpsPosition, p2: GpsPosition) {
    const R = 6371e3; // metres
    const φ1 = p1.lat * Math.PI / 180; // φ, λ in radians
    const φ2 = p2.lat * Math.PI / 180;
    const Δφ = (p2.lat - p1.lat) * Math.PI / 180;
    const Δλ = (p2.lng - p1.lng) * Math.PI / 180;

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) *
      Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c; // in metres
  }

  calculateBearing(p1: GpsPosition, p2: GpsPosition) {
    //φ1,λ1 is the start point, φ2,λ2 the end point (Δλ is the difference in longitude

    const φ1 = p1.lat * Math.PI / 180; // φ, λ in radians
    const φ2 = p2.lat * Math.PI / 180;
    const λ1 = p1.lng * Math.PI / 180; // φ, λ in radians
    const λ2 = p2.lng * Math.PI / 180;

    const y = Math.sin(λ2 - λ1) * Math.cos(φ2);
    const x = Math.cos(φ1) * Math.sin(φ2) -
      Math.sin(φ1) * Math.cos(φ2) * Math.cos(λ2 - λ1);
    const θ = Math.atan2(y, x);
    const brng = (θ * 180 / Math.PI + 360) % 360; // in degrees
    return brng;
  }

  *gpsPoint() {

    const lerp = (x1, x2, t) => (x2 - x1) * t + x1

    let currentIndex = START_INDEX;
    let t0 = data.fixes[currentIndex].timestamp;
    let deltaT = new Date().getTime() - t0;


    while (currentIndex < data.fixes.length - 1) {

      var fix0 = data.fixes[currentIndex];
      var fix1 = data.fixes[currentIndex + 1];

      let t = new Date().getTime() - deltaT;

      if (fix1.timestamp >= t) {

        const interp = (t - fix0.timestamp) / (fix1.timestamp - fix0.timestamp)

        yield {
          lat: lerp(fix0.latitude, fix1.latitude, interp),
          lng: lerp(fix0.longitude, fix1.longitude, interp),
          altitude: lerp(fix0.gpsAltitude, fix1.gpsAltitude, interp),
          timestamp: t
        } as GpsPosition
      } else {
        currentIndex++;
      }

    }

    return {

    } as GpsPosition;

  }

  myio: Socket<DefaultEventsMap, DefaultEventsMap>;

  constructor() {


    this.myio = io();
    console.log(this.myio) 
    this.myio.on("gps", data => {
      this.currentGps = data;
    })

    this.gps = new BehaviorSubject<GpsPosition>({
      lat: data.fixes[0].latitude,
      lng: data.fixes[0].longitude,
      altitude: data.fixes[0].gpsAltitude
    })

    this.groundSpeed = new Vector2(0, 0);

    let time = 0;
    let dt = 0.02;

    this.windSpeed = new Vector2(3, 0).rotate(time / 10);
    this.airSpeed = new Vector2(10, 0).rotate(Math.sin(time / 2) * 2 * Math.PI / 15)
    this.groundSpeed = this.windSpeed.add(this.airSpeed)

    let iter = this.gpsPoint();
    let lastP = iter.next().value;
    var lastAltitude = lastP.altitude;
    this.altitudeFIR = new FIR(30, lastAltitude);
    // this.bearingFIR = new FIR(30, 0);

    setInterval(() => {
      var p = iter.next().value

      if (this.currentGps != null) {
        p.lat = this.currentGps.lat;
        p.lng = this.currentGps.lng;
      }

      this.altitudeFIR.pushValue(p.altitude)
      const thisAltitude = this.altitudeFIR.getValue()
      const dH = (thisAltitude - lastAltitude) / 0.1

      const bearing = this.calculateBearing(lastP, p);
      p.verticalVelocity = dH
      p.bearing = bearing;

      this.gps.next(p)

      this.bearing = bearing;// this.bearingFIR.pushValue(bearing);

      lastAltitude = thisAltitude;
      lastP = p;

    }, 100)

    setInterval(() => {

      this.windSpeed = new Vector2(3, 0).rotate(time / 10);
      this.airSpeed = new Vector2(0, -10).rotate(this.bearing / 180 * Math.PI)
      this.groundSpeed = this.windSpeed.add(this.airSpeed)

      var speeds = [this.windSpeed, this.airSpeed]

      let minX = Math.min(...speeds.map(s => s.x));
      let maxX = Math.max(...speeds.map(s => s.x));
      let minY = Math.min(...speeds.map(s => s.y));
      let maxY = Math.max(...speeds.map(s => s.y));

      this.center = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);

      time += dt;
    }, dt * 1000)
  }
}
