import { Component, OnInit } from '@angular/core';
import { GliderService } from '../glider.service';

@Component({
  selector: '[app-vector-speedometer]',
  templateUrl: './vector-speedometer.component.html',
  styleUrls: ['./vector-speedometer.component.scss']
})
export class VectorSpeedometerComponent implements OnInit {

  constructor(public glider: GliderService) { }

  ngOnInit(): void {
  }

  tf() {
    return `scale(30) translate(${-this.glider.center.x}, ${-this.glider.center.y})`
  }

  gliderTf() {
    return `scale(0.2) rotate(${this.glider.bearing})`
  }

}
