import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VectorSpeedometerComponent } from './vector-speedometer.component';

describe('VectorSpeedometerComponent', () => {
  let component: VectorSpeedometerComponent;
  let fixture: ComponentFixture<VectorSpeedometerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VectorSpeedometerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VectorSpeedometerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
