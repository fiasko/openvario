module.exports = {
  "/hot": {
    "target": "http://192.168.178.115",
    "secure": false,
    "ws": true
  },
  "/socket.io": {
    target: "http://raspberrypi:3000",
    ws: true
  }
}