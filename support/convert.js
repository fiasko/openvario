const fs = require('fs');
const IGCParser = require('igc-parser');

let result = IGCParser.parse(fs.readFileSync('01-1421.IGC', 'utf8'));

fs.writeFileSync('data.json', JSON.stringify(result, null, 2));